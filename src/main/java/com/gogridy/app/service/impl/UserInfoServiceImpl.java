package com.gogridy.app.service.impl;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gogridy.app.domain.Authority;
import com.gogridy.app.domain.UserInfo;
import com.gogridy.app.repository.AuthorityRepository;
import com.gogridy.app.repository.UserInfoRepository;
import com.gogridy.app.service.UserInfoService;
import com.gogridy.app.service.dto.UserInfoDTO;
import com.gogridy.app.service.mapper.UserInfoMapper;


/**
 * Service Implementation for managing UserInfo.
 */
@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService{

    private final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);

    private final UserInfoRepository userInfoRepository;

    private final UserInfoMapper userInfoMapper;
    
    private final PasswordEncoder passwordEncoder;
    
    private final AuthorityRepository authorityRepository;

    public UserInfoServiceImpl(UserInfoRepository userInfoRepository, UserInfoMapper userInfoMapper, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository) {
        this.userInfoRepository = userInfoRepository;
        this.userInfoMapper = userInfoMapper;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    /**
     * Save a userInfo.
     *
     * @param userInfoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserInfoDTO save(UserInfoDTO userInfoDTO) {
        log.debug("Request to save UserInfo : {}", userInfoDTO);
        UserInfo userInfo = userInfoMapper.toEntity(userInfoDTO);
        userInfo = userInfoRepository.save(userInfo);
        return userInfoMapper.toDto(userInfo);
    }

    /**
     *  Get all the userInfos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserInfoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserInfos");
        return userInfoRepository.findAll(pageable)
            .map(userInfoMapper::toDto);
    }

    /**
     *  Get one userInfo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UserInfoDTO findOne(Long id) {
        log.debug("Request to get UserInfo : {}", id);
        UserInfo userInfo = userInfoRepository.findOneWithEagerRelationships(id);
        return userInfoMapper.toDto(userInfo);
    }

    /**
     *  Delete the  userInfo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserInfo : {}", id);
        userInfoRepository.delete(id);
    }

	@Override
	public UserInfo createUser(UserInfoDTO userInfoDTO) {
			UserInfo user = new UserInfo();
	        user.setUserLogin(userInfoDTO.getUserLogin());
	        user.setAddress(userInfoDTO.getAddress());
	        user.setEmail(userInfoDTO.getEmail());
	        user.setPhoneNumber(userInfoDTO.getPhoneNumber());
	        Authority auth = new Authority();
	        if (userInfoDTO.getAuthorities() != null) {
	            Set<Authority> authorities = new HashSet<>();
	            for (Authority authority : authorities) {
					authorities.add(authorityRepository.findByName(authority.getName()));
				}
	            user.setAuthorities(authorities);
	        }
	        String encryptedPassword = passwordEncoder.encode(userInfoDTO.getPassword());
	        user.setPassword(encryptedPassword);
	        user.setCreatedDate(LocalDate.now());
	        user.setUpdatedDate(LocalDate.now());
	        UserInfo userResult = userInfoRepository.save(user);
	        
	        log.debug("Created Information for User: {}", user);
	        return user;
	}
}
