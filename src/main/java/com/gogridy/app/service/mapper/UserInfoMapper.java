package com.gogridy.app.service.mapper;

import com.gogridy.app.domain.*;
import com.gogridy.app.service.dto.UserInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserInfo and its DTO UserInfoDTO.
 */
@Mapper(componentModel = "spring", uses = {AuthorityMapper.class, })
public interface UserInfoMapper extends EntityMapper <UserInfoDTO, UserInfo> {
    
    
    default UserInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setId(id);
        return userInfo;
    }
}
