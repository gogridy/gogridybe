package com.gogridy.app.service.mapper;

import com.gogridy.app.domain.*;
import com.gogridy.app.service.dto.AuthorityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Authority and its DTO AuthorityDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AuthorityMapper extends EntityMapper <AuthorityDTO, Authority> {
    
    
    default Authority fromId(Long id) {
        if (id == null) {
            return null;
        }
        Authority authority = new Authority();
        authority.setId(id);
        return authority;
    }
}
