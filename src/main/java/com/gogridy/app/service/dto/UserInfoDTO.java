package com.gogridy.app.service.dto;


import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import java.util.Objects;

/**
 * A DTO for the UserInfo entity.
 */
public class UserInfoDTO implements Serializable {
	
	public static final int PASSWORD_MIN_LENGTH = 4;

	public static final int PASSWORD_MAX_LENGTH = 100;

    private Long id;

    private String userLogin;

    private String password;

    private String username;

    private String phoneNumber;

    private Integer userParent;

    private String address;

    private String email;

    private String anotherInfo;

    private LocalDate createdDate;

    private LocalDate updatedDate;

    private String userRole;

    private Set<AuthorityDTO> authorities = new HashSet<>();
    
    public UserInfoDTO() {
	}
    

    public UserInfoDTO(Long id, String userLogin, String password, String username, String phoneNumber,
			Integer userParent, String address, String email, String anotherInfo, LocalDate createdDate,
			LocalDate updatedDate, String userRole, Set<AuthorityDTO> authorities) {
		super();
		this.id = id;
		this.userLogin = userLogin;
		this.password = password;
		this.username = username;
		this.phoneNumber = phoneNumber;
		this.userParent = userParent;
		this.address = address;
		this.email = email;
		this.anotherInfo = anotherInfo;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.userRole = userRole;
		this.authorities = authorities;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getUserParent() {
        return userParent;
    }

    public void setUserParent(Integer userParent) {
        this.userParent = userParent;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAnotherInfo() {
        return anotherInfo;
    }

    public void setAnotherInfo(String anotherInfo) {
        this.anotherInfo = anotherInfo;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Set<AuthorityDTO> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<AuthorityDTO> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserInfoDTO userInfoDTO = (UserInfoDTO) o;
        if(userInfoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userInfoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserInfoDTO{" +
            "id=" + getId() +
            ", userLogin='" + getUserLogin() + "'" +
            ", password='" + getPassword() + "'" +
            ", username='" + getUsername() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", userParent='" + getUserParent() + "'" +
            ", address='" + getAddress() + "'" +
            ", email='" + getEmail() + "'" +
            ", anotherInfo='" + getAnotherInfo() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", userRole='" + getUserRole() + "'" +
            "}";
    }
}
