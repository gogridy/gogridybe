package com.gogridy.app.web.rest.vm;

import java.time.LocalDate;
import java.util.Set;

import com.gogridy.app.service.dto.AuthorityDTO;
import com.gogridy.app.service.dto.UserInfoDTO;


/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserInfoDTO {

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

	public ManagedUserVM(Long id, String userLogin, String password, String username, String phoneNumber,
			Integer userParent, String address, String email, String anotherInfo, LocalDate createdDate,
			LocalDate updatedDate, String userRole, Set<AuthorityDTO> authorities) {
		
		super(id, userLogin, password, username, phoneNumber, userParent, address, email, anotherInfo, createdDate, updatedDate,
				userRole, authorities);
		// TODO Auto-generated constructor stub
	}

	@Override
    public String toString() {
        return "ManagedUserVM{" +
            "} " + super.toString();
    }
}
