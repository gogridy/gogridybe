/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gogridy.app.web.rest.vm;
