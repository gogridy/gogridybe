package com.gogridy.app.repository;

import com.gogridy.app.domain.UserInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the UserInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
    @Query("select distinct user_info from UserInfo user_info left join fetch user_info.authorities")
    List<UserInfo> findAllWithEagerRelationships();

    @Query("select user_info from UserInfo user_info left join fetch user_info.authorities where user_info.id =:id")
    UserInfo findOneWithEagerRelationships(@Param("id") Long id);
    
    Optional<UserInfo> findOneByEmail(String email);
    
    Optional<UserInfo> findOneByUserLogin(String login);

}
